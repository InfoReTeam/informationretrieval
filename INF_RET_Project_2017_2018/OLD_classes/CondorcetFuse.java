import java.io.*; 
import java.util.*; 

public class CondorcetFuse 
{
  private static int[][] array = null; // lo rendo visibile a tutti i metodi della classe, lo inizializzo nel main 
  private static final int numberOfRuns = 4; 
  private static final int numberOfQueries = 5; 
  private static final int numberOfDocuments = 6;
  private Hashtable<Integer, TupleQD[]> hash = new Hashtable<Integer, TupleQD[]>(); 

  public static void main(String[] args) 
  {
    //Inizializzazione strutture dati 
    int numberOfRows = numberOfDocuments*numberOfQueries; 
    array = new int[numberOfRows][numberOfRuns]; 
     
    // Inizializzo tutti i campi dell'array bidimensionale a -1 
    for(int i = 0; i < numberOfRows; i++)
    {
    	for(int j=0; j < numberOfRuns; j++)
    	{
    		array[i][j] = -1 ; 
    	}
    }
    
    // Vettore di liste di documenti, una lista per query, ciascuna lista verrà poi riordinata tramite quickSort 
    Vector<TupleQD[]> vector = new Vector<TupleQD[]>(); 
    // lista ordinata di tuple per la query 1 --> vector.get(0)
    // lista ordinata di tuple per la query 2 --> vector.get(1) etc...
    for(int t = 0; t < numberOfQueries; t++)
    {
    	TupleQD[] tuple = new TupleQD[numberOfDocuments]; 
    	for(int k = 0; k < numberOfDocuments; k++)
    	{
    		tuple[k] = new TupleQD((t+1), k); 
    	}
    	vector.add(t, tuple); 
    }

    // Inserimento dei ranking dei documenti nell'array
    File[] runs = new File[numberOfRuns]; 
    Scanner sc = null; 
    Scanner scLine = null; 

    String path0 = "/home/alessandro/Scrivania/terrier-core-4.1/var/results/TF_IDF_13.res";
	  runs[0] = new File(path0); 
	  String path1 = "/home/alessandro/Scrivania/terrier-core-4.1/var/results/DLH13_Bo1bfree_d_3_t_10_12.res";
	  runs[1] = new File(path1); 
	  String path2= "/home/alessandro/Scrivania/terrier-core-4.1/var/results/DLH13_9.res";
	  runs[2] = new File(path2); 
	  String path3 = "/home/alessandro/Scrivania/terrier-core-4.1/var/results/BM25b0.75_6.res";
	  runs[3] = new File(path3); 
        
    int queryNumber = 0; 
    int docNumber = 0; 
    int rank = 0;
    int rowIndex = 0; 
     
    //Inizializzazione dei rank dei documenti nell'array 
    for(int q=0; q < runs.length; q++)
    {
    	try
		  {
		    sc = new Scanner(runs[q]); 
	    }
		  catch(FileNotFoundException ioe)
		  {
	      ioe.printStackTrace(); 
		    System.exit(1); 
		  }
        
		  while(sc.hasNextLine())
		  {
		    String tmp = sc.nextLine();
		    scLine = new Scanner(tmp); 
		    String token = scLine.next(); 
		    queryNumber = Integer.parseInt(token.substring(7)); 
		    // Skip Q0 
		    scLine.next(); 
		    String docID = scLine.next(); 
		    docNumber = Integer.parseInt(docID.substring(10)); 
        rank = Integer.parseInt(scLine.next());  
        rowIndex = ((queryNumber-1)*numberOfDocuments) + docNumber; 
        array[rowIndex][q] = rank;  
		  }
    }//end for

      
    //Debugging*************************************************************************************  
    /*
    // Controllo valori array 
    for(int i=0; i<(numberOfDocuments*numberOfQueries); i++)
    { 
      for(int j=0; j<(numberOfRuns-1); j++)
      {
        System.out.print(" "+Integer.toString(array[i][j]) + "    ");
      }
      System.out.println(Integer.toString(array[i][numberOfRuns-1])); 
    }
    */
    //**********************************************************************************************

    int match = (-1)*numberOfRuns;
    int actQueryNumber = 0; 
    Hashtable<Integer, Integer> queryNotRelevant = new Hashtable<Integer, Integer>(); 

    for(int k=1; k<numberOfQueries+1; k++)
    {
      queryNotRelevant.put(Integer.valueOf(k), new Integer(0)); 
    }

    int i = 0; 
    int sum = 0; 
    for(i = 0; i < numberOfRows; i++)
    {
      sum = 0; 

      if((i%numberOfDocuments)==0)
      {
        actQueryNumber = actQueryNumber+1 ; 
      }

      for(int y=0; y<numberOfRuns; y++)
      {
        sum = sum + array[i][y]; 
      }

      if(Integer.compare(sum, match) == 0)
      {
        int act = queryNotRelevant.get(Integer.valueOf(actQueryNumber)).intValue(); 
        act = act+1; 
        queryNotRelevant.replace(Integer.valueOf(actQueryNumber), Integer.valueOf(act)); 
      }
    }
   
    TupleQD[] tuple = null; 
    for(int k=0; k<vector.size(); k++)
    { 
      tuple = vector.get(k); 
      quickSort(tuple, 0, tuple.length-1); 
    } 
    
    //Rimuovo gli elementi non rilevanti dagli array finali
    int numNotRel = 0;
    int tmp = 0; 
    int t=0; 
    for(t=0; t<vector.size(); t++)
    {
      tmp = (t+1); 
      numNotRel = (queryNotRelevant.get(Integer.valueOf(tmp))).intValue(); 
      TupleQD[]  newTuple = Arrays.copyOfRange(vector.get(t), 0, (vector.get(t).length)-numNotRel);  
      vector.set(t, newTuple); 
    } 

    //Debugging*******************************************************************************
    //**************************************************************************************** 
    
    for(t=0; t<vector.size(); t++)
    {
      //System.out.println(Arrays.toString(vector.get(t))); 
      TupleQD[] result = vector.get(t); 
      System.out.println("*********"+"Query number: " + result[0].getQuery() + "*********"); 
      for(int k=0; k<result.length; k++)
      {
        System.out.println("DocID: " + Integer.toString(result[k].getDocument())); 
      }  
    }

    /*
    Enumeration<TupleQD[]> enumeration = hash.keys(); 
    while(enumeration.hasMoreElements()) 
    {
      TupleQD[] next = enumeration.nextElement(); 
      int counter = (hash.get(next)).intValue(); 
      System.out.println("Valore contatore: " + counter); 
    }

    System.out.println("Valori array CountNotRelevant**************"); 
    for(int y=0; y<countNotRelevant.length; y++)
    {
      System.out.println(Integer.toString(countNotRelevant[y]));
    */
    //**************************************************************************************** 

  }//[m]main

// Non elimino l'elemento con tutti -1 tanto tale elemento perderà contro tutti
// e si troverà in fondo all'array --> per ogni array basta tenere un contatore del numero di 
// elementi che si trovano in coda e che possono essere rimossi in quanto non rilevanti 

//QuickSort 
public static void quickSort(TupleQD[] arr, int left, int right) 
{
    int i = left, j = right;
    TupleQD tmp = null; 
    int arrIndex = (left + right)/2; 
    TupleQD pivot = arr[arrIndex]; 
    int count = 0; 
    int row = (((pivot.getQuery()-1)*numberOfDocuments) + pivot.getDocument()); 
    
       while (i <= j) 
       {   
       	   boolean ciclo1 = true; 
       	   while(ciclo1)
       	   {  //(arr[i],pivot) --> count > 0, arr[i] vince , count < 0 arr[i] perde 
       	   	  count = 0; 
       	      int rowOfi = ((arr[i].getQuery()-1)*numberOfDocuments) + arr[i].getDocument(); 
              for(int t=0; t < numberOfRuns; t++)
              {
              	if(array[rowOfi][t] == -1 || array[row][t] == -1)
              	{
              	 	  if(array[rowOfi][t] == array[row][t]) 
                    {}
              	    else if(array[rowOfi][t] == -1) 
              	    {
              	    	count--; //vince il pivot 
              	    }
              	    else if(array[row][t] == -1)
              	    {
              	    	count++; // vince array[i] 
              	    } 	
              	}
              	else 
              	{
              		if(array[rowOfi][t] < array[row][t]) //vince arr[i] sulla t-esima run 
              		{
              			count++; 
              		}
              		else if(array[rowOfi][t] > array[row][t]) //vince il pivot sulla t-esima run 
              		{
              			count--; 
              		}
              	}
              }//end for
              
              //Gli elementi che stanno alla sx del pivot hanno un rank più alto del pivot
              //Attenzione! Rank più alto significa numericamente minore del pivot 
              if(count > 0) // arr[i] ha vinto e sta alla sx del pivot --> ok! 
              {              // Oppure count==0 --> in questo caso l'ordinamento tra i due sfidanti è indifferente, opto per lo swap!
                i++; 
              }
              else 
              {
              	ciclo1 = false;  
              }
           }//end while(ciclo1)     

       	   boolean ciclo2 = true; 
       	   while(ciclo2)
       	   {  //(arr[j],pivot) --> count > 0 arr[j] vince , count < 0 arr[j] perde 
       	   	  count = 0; 
       	      int rowOfj = ((arr[j].getQuery()-1)*numberOfDocuments) + arr[j].getDocument(); 
              for(int t=0; t < numberOfRuns; t++)
              {
              	if((array[rowOfj][t] == -1) || (array[row][t] == -1))
              	{
              	 	if(array[rowOfj][t] == array[row][t]) {}
              	    else if(array[rowOfj][t] == -1) 
              	    {
              	    	count--; //vince il pivot 
              	    }
              	    else if(array[row][t] == -1)
              	    {
              	    	count++; // vince array[j] 
              	    }
              	 	
              	}
              	else 
              	{
              		if(array[rowOfj][t] < array[row][t]) //arr[j] vince rispetto al pivot 
              		{
              			count++; 
              		}
              		else if(array[rowOfj][t] > array[row][t]) //il pivot vince rispetto ad arr[j]
              		{
              			count--; 
              		}
              	}
              }//end for

              if(count < 0) //arr[j] ha perso e sta alla dx del pivot --> ok! 
              {              //se count==0 l'ordinamento tra il pivot ed arr[j] è indifferente, opto per lo swap!  
                j--;
              }
              else 
              {
              	ciclo2 = false;  
              }
           }//end while(ciclo2) 
          

           if(i <= j)  
           {
		          tmp = arr[i];
              arr[i] = arr[j];
              arr[j] = tmp;
              i++;
              j--;
           }
       }//end while(i<=j)
      
       int idx = i; 

       if (left < (idx-1))
           quickSort(arr, left, idx-1);

       if (idx < right)
           quickSort(arr, idx, right);

}//[m]QuickSort

}//{c} CondorcetFuse 