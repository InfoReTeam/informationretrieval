package condorcetFuseIndexes; 
import java.io.*; 
import java.util.*; 

public class ReadFromRes 
{
	private static final int numberOfRuns = 10; 
    private static final int numberOfQueries = 50; 
	private static final int numberOfDocuments = 528155; 
	private static final int numInizialeQuery = 351; 
	private static final int numFinaleQuery = 400; 

	public static void main(String[] args) 
	{
		File[] runs = new File[numberOfRuns]; 
        Scanner sc = null; 
        Scanner scLine = null; 

        PrintWriter pwSUM = null;
        PrintWriter pwMAX = null; 
        PrintWriter pwMIN = null;
		PrintWriter pwMED = null;
		PrintWriter pwANZ = null; 
		PrintWriter pwMNZ = null; 

        try
        {
			pwSUM = new PrintWriter(new FileOutputStream("combSUM_TestingNormalized_22_12_2017"));
			pwMAX = new PrintWriter(new FileOutputStream("combMAX_TestingNormalized_22_12_2017"));
			pwMIN = new PrintWriter(new FileOutputStream("combMIN_TestingNormalized_22_12_2017"));
			pwMED = new PrintWriter(new FileOutputStream("combMED_TestingNormalized_22_12_2017"));
			pwANZ = new PrintWriter(new FileOutputStream("combANZ_TestingNormalized_22_12_2017"));
			pwMNZ = new PrintWriter(new FileOutputStream("combMNZ_TestingNormalized_22_12_2017"));
        }
        catch(FileNotFoundException ioe)
        {
        	ioe.printStackTrace(); 
        } 

		// Si può anche(molto meglio) preparare un file con tutti i path che portano 
		// ai vari file .res e poi importarli con scanner in un ciclo for 
		String abspath = "/home/alessandro/Scrivania/terrier-core-4.2/var/results/file_Results_Project/";
        String path0 = abspath + "1_BM25/BM25b0.75_0.res";
        runs[0] = new File(path0); 
        String path1 = abspath + "2_TF_IDF/TF_IDF_0.res";
        runs[1] = new File(path1); 
        String path2= abspath + "11_DFR_BM25/DFR_BM25c1.0_1.res";
        runs[2] = new File(path2); 
        String path3 = abspath + "4)Hiemstra_LM/Hiemstra_LM0.15_0.res";
        runs[3] = new File(path3); 
        String path4 = abspath + "5)DirichletLM/DirichletLM_0.res";
        runs[4]= new File(path4); 
        String path5 = abspath + "6)BM25/BM25b0.75_0.res";
        runs[5] = new File(path5); 
        String path6 = abspath + "7_TF_IDF/TF_IDF_0.res";
        runs[6] = new File(path6); 
        String path7 = abspath + "12_Tf/Tf_0.res";
        runs[7] = new File(path7); 
        String path8 = abspath + "9_Hiemstra_LM/Hiemstra_LM0.15_0.res"; 
        runs[8] = new File(path8); 
        String path9 = abspath + "10_DirichletLM/DirichletLM_0.res"; 
        runs[9] = new File(path9);      

        /*
        Vector<String> files = null;
        // Inserimento dei ranking dei documenti nell'array
        try
        {
           files = Util.fileInFolder(absPath);
        }
        catch(IOException ie)
        {
           ie.printStackTrace();
           System.exit(1);
        }
    
        //numRuns = files.size();
        //File[] runs = new File[files.size()];
        for(int i = 0; i < files.size(); i++)
           runs[i] = new File(absPath + files.get(i));

        */

        int queryNumber = 0; 
        String docID = null;
        double score = 0.0; 
 
        HashMap<Tuple, Double> hashtableCombSUM = new HashMap<Tuple, Double>(); 
        HashMap<Tuple, Double> hashtableCombMAX = new HashMap<Tuple, Double>(); 
        HashMap<Tuple, Double> hashtableCombMIN = new HashMap<Tuple, Double>();
		HashMap<Tuple, Vector<Double>> hashtableCombMED = new HashMap<Tuple, Vector<Double>>();
		
        // hashtable per memorizzare il numero di similarita' del documento docID non nulle 
        // mappo ciascun documento identificato con docID e la query corrispondente nel numero di similarita' non nulle  
        // mi serve per il calcolo della combANZ e della combMNZ 
        HashMap<Tuple,Integer> docSimilarities = new HashMap<Tuple, Integer>();
		
        for(int i=0; i<runs.length;i++)
        {
        	try
		    {
			  sc = new Scanner(runs[i]); 
		    }
		    catch(FileNotFoundException ioe)
		    {
			  ioe.printStackTrace(); 
			  System.exit(1); 
		    }
            
            // ArrayList temporaneo per salvare gli score normalizzati che poi verranno salvati in modo 
            // opportuno nelle tabelle hash 
            ArrayList<PairTupleScore> list = new ArrayList<PairTupleScore>(); 
            int counter = 0; 
		    // La normalizzazione la eseguo in modo indipendente per ciascuna run 
		    // metodo di normalizzazione utilizzato Min_Max = (old_sim-minimum_sim)/(maximum_sim-minimum_sim)
		    // Normalizzazione 
		    //***************************************************************************************
		    while(sc.hasNextLine())
		    {
			  String tmp = sc.nextLine();
		      scLine = new Scanner(tmp); 
		
			  String token = scLine.next(); 
			  queryNumber = Integer.parseInt(token); //DA CAMBIARE QUANDO SI VA A LAVORARE CON LE NUOVE RUN!!!!!!! 
			  // Skip Q0 
			  scLine.next(); 
			  docID = scLine.next();  
			  //skip rank 
			  scLine.next(); 
			  // score 
			  score = Double.parseDouble(scLine.next()); 

              PairTupleScore tupleScore = new PairTupleScore(new Tuple(docID, queryNumber), score); 
			  list.add(counter++, tupleScore);
			}

			int count = 0; // primo contatore per la lista, non viene mai decrementato 
			int counterList = 0; // contatore per la lista, non viene mai decrementato 

			for(int j = numInizialeQuery; j <= numFinaleQuery; j++)
			{
				PairTupleScore[] tmp = new PairTupleScore[numberOfDocuments];
				int countTmp = 0;
				while(list.get(count).getTuple().getQueryId() == j) 
				{
					tmp[countTmp] = list.get(count); 
					countTmp++; 
					count++; 

					if(count >= list.size())
					{
					   break; 
					}
				} 

				// l'ultimo oggetto si trova all'indice countTmp-1 
				double max = tmp[0].getScore(); 
				double min = tmp[countTmp-1].getScore(); 
				double min_max_normalizedScore = 0.0;
				for(int k=0; k<countTmp; k++)
				{
					//Considera casi patologici
					if(Double.compare(max, min) == 0)
					{
						min_max_normalizedScore =  0.0;
					}
					else
					{
						min_max_normalizedScore =  (tmp[k].getScore()-min)/(max-min);

					}
					PairTupleScore newPair = new PairTupleScore(tmp[k].getTuple(), min_max_normalizedScore);
				    list.set(counterList, newPair);
				    counterList++; 
				}
			}
		    //***************************************************************************************
            
            Iterator<PairTupleScore> iterator = list.iterator(); 
		    while(iterator.hasNext()) 
		    {
				PairTupleScore pairTupleScore = iterator.next();
				queryNumber = pairTupleScore.getTuple().getQueryId();
				docID = pairTupleScore.getTuple().getDocumentId();
				score = pairTupleScore.getScore();
		        Tuple tupleTmp = pairTupleScore.getTuple(); 

				//preprocessing for combANZ
				if(!(docSimilarities.containsKey(tupleTmp)) && (Double.compare(score,0.0) != 0))
				{
					docSimilarities.put(tupleTmp, new Integer(1));
				}
				else if((Double.compare(score,0.0) != 0))//vado ad incrementare di uno il numero di similarities non nulle di tale documento
				{
					Integer resultInt = docSimilarities.get(tupleTmp);
					int result = resultInt.intValue();
					result++;
					docSimilarities.replace(tupleTmp, Integer.valueOf(result));
				}
   
				// Utilizzando una hashtable per motivi di efficienza mappo coppie di valori del tipo (query, docID) in uno score
				// che rappresenta lo score del documento docID per quella specifica query
				// Se non esiste la coppia (query, docID) per un qualche documento significa che lo score di tale coppia e' pari a 0.0
               
				// CombSUM --> Sum of all individual similarities
				combSUM(hashtableCombSUM, queryNumber, docID, score);
              
				// CombMAX --> MAX of all individual similarities
				combMAX(hashtableCombMAX, queryNumber, docID, score);
              
				//combMIN --> MIN of all individual similarities
				combMIN(hashtableCombMIN, queryNumber, docID, score);
				
				//Memorizing all the pairs query-documents
				precombMED(hashtableCombMED, queryNumber, docID, score);
			}//end while
        }//end for 
		
		//Computing combMED
		HashMap<Tuple, Double> hash_combMED = new HashMap<Tuple, Double>(); 
		combMED(hashtableCombMED, hash_combMED); 

        //combANZ --> per il calcolo mi servono l'hashtable di combSUM e l'hashtable docSimilarities 
        // piu' una hashtable su cui salvare i risultati ottenuti 
        HashMap<Tuple, Double> hash_combANZ = new HashMap<Tuple, Double>(); 
        combANZ(hash_combANZ, hashtableCombSUM, docSimilarities); 
        // a questo punto la tabella hash hash_combANZ contiene gli score desiderati 
        
        //combMNZ --> sumOfIndividualSimilarities*numberOfNonZeroSimilarities 
        HashMap<Tuple, Double> hash_combMNZ = new HashMap<Tuple, Double>(); 
        combMNZ(hash_combMNZ, hashtableCombSUM, docSimilarities); 


        //*****************************************************************************************************
        //*******Testing and Debugging*************************************************************************
        //*****************************************************************************************************
        
		//combSUM testing and debugging + sorting test 
		ArrayList<PairTupleScore> combSumArr = fromHashToOrderedArray(hashtableCombSUM); 
		Util.fromArrayListToTRECFormat(combSumArr, "combSUM" , "combSUM_Output.res");  
		for(int i = 0; i < combSumArr.size(); i++)
		{
            pwSUM.println(combSumArr.get(i).toString()); 
			pwSUM.flush(); 
		}
        
		//combMAX testing and debugging 
		ArrayList<PairTupleScore> combMaxArr = fromHashToOrderedArray(hashtableCombMAX); 
		Util.fromArrayListToTRECFormat(combMaxArr, "combMAX" , "combMAX_Output.res"); 
		for(int i = 0; i < combMaxArr.size(); i++)
		{
            pwMAX.println(combMaxArr.get(i).toString());  
			pwMAX.flush(); 
		}

		//combMIN testing and debugging 
		ArrayList<PairTupleScore> combMinArr = fromHashToOrderedArray(hashtableCombMIN); 
		Util.fromArrayListToTRECFormat(combMinArr, "combMIN" , "combMIN_Output.res"); 
		for(int i = 0; i < combMinArr.size(); i++)
		{
            pwMIN.println(combMinArr.get(i).toString()); 
			pwMIN.flush(); 
		}
		
		//combMED testing and debugging
		ArrayList<PairTupleScore> combMedArr = fromHashToOrderedArray(hash_combMED); 
		Util.fromArrayListToTRECFormat(combMedArr, "combMED" , "combMED_Output.res"); 
		for(int i = 0; i < combMedArr.size(); i++)
		{
			pwMED.println(combMedArr.get(i).toString());
			pwMED.flush();
		}

		//combANZ testing and debugging
		ArrayList<PairTupleScore> combAnzArr = fromHashToOrderedArray(hash_combANZ); 
		Util.fromArrayListToTRECFormat(combAnzArr, "combANZ" , "combANZ_Output.res"); 
		for(int i = 0; i < combAnzArr.size(); i++)
		{
			pwANZ.println(combAnzArr.get(i).toString());
			pwANZ.flush();
		}

		//combMNZ testing and debugging
		ArrayList<PairTupleScore> combMnzArr = fromHashToOrderedArray(hash_combMNZ); 
		Util.fromArrayListToTRECFormat(combMnzArr, "combMNZ" , "combMNZ_Output.res"); 
		for(int i = 0; i < combMnzArr.size(); i++)
		{
			pwMNZ.println(combMnzArr.get(i).toString());
			pwMNZ.flush();
		}


		//*****************************************************************************************************
		//*****End of Test and Debugging************************
		//******************************************************

	}//[m]main


	// CombSUM --> Sum of all individual similarities
	private static void combSUM(HashMap<Tuple, Double> hashtable, int queryNumber, String docID, double score)
	{
		Tuple tuple = new Tuple(docID, queryNumber); 
    
		if(hashtable.containsKey(tuple))
		{
			double value = (hashtable.get(tuple)).doubleValue();
			double sum = Double.sum(value, score);
			hashtable.replace(tuple, Double.valueOf(sum));
		}
		else
		{
			hashtable.put(tuple, Double.valueOf(score));
		}
              	 
	}//[m]combSUM


	// CombMAX --> MAX of all individual similarities
	private static void combMAX(HashMap<Tuple,Double> hashtable, int queryNumber, String docID, double score)
	{
		Tuple tuple = new Tuple(docID, queryNumber);

		if(hashtable.containsKey(tuple))
		{
			double value = (hashtable.get(tuple)).doubleValue();
			double max = Double.max(value, score);
			hashtable.replace(tuple, Double.valueOf(max));
		}
		else
		{
			hashtable.put(tuple, Double.valueOf(score));
		}
	}//[m]combMAX

	//combMIN --> MIN of all individual similarities
	private static void combMIN(HashMap<Tuple,Double> hashtable, int queryNumber, String docID, double score)
	{
		Tuple tuple = new Tuple(docID, queryNumber); 
    
		if(hashtable.containsKey(tuple))
		{
			double value = (hashtable.get(tuple)).doubleValue();
			double min = Double.min(value, score);
			hashtable.replace(tuple, Double.valueOf(min));
		}
		else
		{
			hashtable.put(tuple, Double.valueOf(score));
		}

	}//[m]combMIN

	//combANZ --> Sum(all individual similarities) / (#of nonzero similarities)
	private static void combANZ(HashMap<Tuple, Double> hash_combANZ, HashMap<Tuple, Double> hashtable,HashMap<Tuple,Integer> docSimilarities)
	{
		Iterator<Tuple> enumeration = hashtable.keySet().iterator();
		while(enumeration.hasNext())
		{
	 		Tuple next = enumeration.next();
			double sumOfSimilarities = (hashtable.get(next)).doubleValue();

			if(docSimilarities.containsKey(next))
			{
				int numberOfNonZeroSimilarities = (docSimilarities.get(next)).intValue();
				double valueForCombANZ = (sumOfSimilarities/numberOfNonZeroSimilarities);
				hash_combANZ.put(next, Double.valueOf(valueForCombANZ));
			}
		}

	}//[m]combANZ

	//combMNZ --> sumOfIndividualSimilarities*numberOfNonZeroSimilarities
	private static void combMNZ(HashMap<Tuple, Double> hash_combMNZ, HashMap<Tuple, Double> hashtable,HashMap<Tuple,Integer> docSimilarities)
	{
		Iterator<Tuple> enumeration = hashtable.keySet().iterator();
		while(enumeration.hasNext())
		{
			Tuple next = enumeration.next();
			double sumOfSimilarities = (hashtable.get(next)).doubleValue();

			if(docSimilarities.containsKey(next))
			{
				int numberOfNonZeroSimilarities = (docSimilarities.get(next)).intValue();
				double valueForCombMNZ = (sumOfSimilarities * numberOfNonZeroSimilarities);
				hash_combMNZ.put(next, Double.valueOf(valueForCombMNZ));
			}
		}
	}//[m]combMNZ


	/*
	 * @brief precombMED acquires one triplette at a time and it inserts it into the hashtable
	 *
	 * @version 1.0
	 */
	private static void precombMED(HashMap<Tuple, Vector<Double>> queryDocScore, int queryNumber, String docID, double score)
	{
		Tuple tmp = new Tuple(docID, queryNumber); 
		Vector<Double> tmpScore = null;
		if(!queryDocScore.containsKey(tmp))
		{
			tmpScore = new Vector<Double>();
			tmpScore.add(score);
			queryDocScore.put(tmp, tmpScore);
		}
		else
		{
			tmpScore = queryDocScore.get(tmp);
			tmpScore.add(score);
			queryDocScore.replace(tmp, tmpScore); 
		}
	}//[m]precombMED
	
	/*
	 * @brief combMED computes the median over each pair document-query and stores it into the hashtable hash_combMED 
	 *
	 * @version 1.0
	 */
	private static void combMED(HashMap<Tuple, Vector<Double>> hashTable, HashMap<Tuple, Double> hash_combMED)
	{
		for(Map.Entry<Tuple, Vector<Double>> entry : hashTable.entrySet())
		{
			Vector<Double> vecTmp = entry.getValue();
			vecTmp.sort(null);  
			int middle = (vecTmp.size()/2) ; 
			Double tmpScore = vecTmp.elementAt(middle); 
			hash_combMED.put(entry.getKey(), tmpScore); 
		}
	}//[m]combMED

    /*
    *@Brief Take an hashtable as input and return an ordered array as output 
    */

    private static ArrayList<PairTupleScore> fromHashToOrderedArray(HashMap<Tuple, Double> hashtable)
    {
    	Vector<ArrayList<PairTupleScore>> vec = new Vector<ArrayList<PairTupleScore>>(); 

    	for(int j = 0; j < numberOfQueries; j++)
    	{
    		vec.add(new ArrayList<PairTupleScore>()); 
    	}
        
        Iterator<Tuple> enumeration = hashtable.keySet().iterator(); 
        while(enumeration.hasNext())
        {
        	Tuple tpl = enumeration.next(); 
        	double score = hashtable.get(tpl).doubleValue(); 
        	PairTupleScore tempPair = new PairTupleScore(tpl, score); 
        	(vec.get(tpl.getQueryId()-numInizialeQuery)).add(tempPair); 
        }
           

        //Riordino gli ArrayList così ottenuti 
        for(int i = 0; i < vec.size(); i++)
        {
        	vec.get(i).sort(null); 
        }
       
        ArrayList<PairTupleScore> res = vec.get(0); 

        for(int k = 1; k < vec.size(); k++)
        {
        	for(int t = 0; t < vec.get(k).size(); t++)
        	{
        		res.add(vec.get(k).get(t)); 
        	}
        }

        return res; 
    } 


}//{c}ReadFromRes
