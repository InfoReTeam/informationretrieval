package condorcetFuseIndexes; 
import java.util.Objects; 

public class Tuple //Tuple with DocID and queryID   
{
	private String docID; 
	private int queryID; 

	public Tuple(String docID, int queryID)
	{
		this.queryID = queryID; 
		this.docID = docID; 
	}

	public int getQueryId()
	{
		return queryID; 
	}

	public String getDocumentId()
	{
		return docID; 
	}

    // equals() and hashCode() must be overrided if we want to use Hashtable properly
	@Override
	public String toString()
	{
		return ("QueryID: " + queryID + ", docID: " + docID);   
	}
    
    @Override 
    public int hashCode()
    {
    	return Objects.hash(queryID, docID);  
    }

    @Override
    public boolean equals(Object obj)
    {
    	if(obj==this) return true; 
    	if(!(obj instanceof Tuple)) 
    	{
    		return false; 
    	} 

    	Tuple tuple = (Tuple)obj; 

    	return queryID==tuple.queryID && docID.equals(tuple.docID); 
    }
    
}//{c}Tuple