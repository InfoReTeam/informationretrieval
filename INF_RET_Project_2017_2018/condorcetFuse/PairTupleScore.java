package condorcetFuseIndexes; 
import java.lang.Comparable;
import condorcetFuseIndexes.*; 

public class PairTupleScore implements Comparable<PairTupleScore>
{
	private Tuple tuple; 
	private double score; 

	public PairTupleScore(Tuple tuple, double score)
	{
		this.tuple = tuple; 
		this.score = score; 
	}

	public Tuple getTuple()
	{
		return tuple; 
	}

	public double getScore()
	{
		return score; 
	}

	public void setScore(double new_score)
	{
		score = new_score; 
	}

    @Override
	public String toString()
	{
		return (tuple.toString() + " Score: " + score); 
	}


	public int compareTo(PairTupleScore e)
	{
		return (-1)*Double.compare(this.score,e.getScore()); //ordinamento decrescente 
	}

}
