//package condorcetFuseIndexes;
import java.util.*; 
import java.io.*; 

public class Util  
{
   private static PrintWriter pw = null; 
   private static int numberOfDocuments = 528155;

   /**
   *@title Metodo per la conversione dei risultati ottenuti tramite gli algoritmi CondorcetFuse in formato TREC
   *@version 2.0 
   *@date 26/11/2017
   *@input params: 
   *  - Vector<Tuple[]> : contiene una lista di documenti ordinati per query 
   *  - String : nome del file di output in cui si desidera salvare i risultati della RUN in formato TREC      
   *  - String : nome del modello utilizzato per ottenere la RUN                              
   */
   public static void fromVecToTRECFormat(Vector<Tuple[]> inputRes, String model, String fileOutputName) 
   {
      try
      {
      	 pw = new PrintWriter(new FileOutputStream(fileOutputName)); 
      }
      catch(FileNotFoundException fne)
      {
      	fne.printStackTrace(); 
      }

      for(int j = 0; j < inputRes.size(); j++)
      {
      	Tuple[] arr = inputRes.get(j); 
      	int counter = 0; 
        int countSim = numberOfDocuments;

      	for(int t = 0; t < arr.length; t++)
      	{
      	 	Tuple tp = arr[counter]; 
      	 	int queryId = tp.getQueryId(); 
      	 	String docId = tp.getDocumentId(); 

      	 	String resLine = (queryId + " Q0 " + docId + " " + counter + " " + countSim + " " + model); 
      	 	counter++; 
          countSim--; 
      	 	pw.println(resLine); 
      	}
      }

    pw.flush(); 
   }


   /**
   *@title Come il precedente metodo, con la sola differenza che prendo in input un ArrayList
   */
   public static void fromArrayListToTRECFormat(ArrayList<PairTupleScore> inputRes, String model, String fileOutputName)
   {
   	  try
      {
      	 pw = new PrintWriter(new FileOutputStream(fileOutputName)); 
      }
      catch(FileNotFoundException fne)
      {
      	fne.printStackTrace(); 
      }
       
      int counter = 0; 
      int actualQueryId = -1; 

      for(int j = 0; j < inputRes.size(); j++)
      {
      	PairTupleScore ptscore = inputRes.get(j); 
      	int queryId = ptscore.getTuple().getQueryId(); 
      	String docId = ptscore.getTuple().getDocumentId(); 
      	double score = ptscore.getScore(); 
       
        if(queryId == actualQueryId)
        {
        	counter++; 
        }
        else 
        {
        	actualQueryId = queryId; 
        	counter = 0; 
        }

      	String resLine = (queryId + " Q0 " + docId + " " + counter + " " + score + " " + model);  
      	pw.println(resLine); 
      }

    pw.flush(); 
   }


   
   public static void fromTupleArrayListToTRECFormat(ArrayList<Tuple> inputRes, String model, String fileOutputName)
   {
   	  try
      {
      	 pw = new PrintWriter(new FileOutputStream(fileOutputName)); 
      }
      catch(FileNotFoundException fne)
      {
      	fne.printStackTrace(); 
      }
       
      int counter = 0; 
      int actualQueryId = -1; 
      int countSim = 0; 

      for(int j = 0; j < inputRes.size(); j++)
      {
      	Tuple ptscore = inputRes.get(j); 
      	int queryId = ptscore.getQueryId();  
      	String docId = ptscore.getDocumentId(); 
       
        if(queryId == actualQueryId)
        {
        	counter++; 
          countSim--; 
        }
        else 
        {
        	actualQueryId = queryId; 
        	counter = 0; 
          countSim = numberOfDocuments; 
        }

      	String resLine = (queryId + " Q0 " + docId + " " + counter + " " + countSim + " " + model);  
      	pw.println(resLine); 
      }

      pw.flush(); 
   }

	public static Vector<String> fileInFolder(String folder) throws IOException{
		Vector<String> files = new Vector<String>();
		File dir = new File(folder);
		File[] f = dir.listFiles();
		
		for(File current : f){
			String tmp = current.getName();
			if(tmp.matches(".*res")) {
				files.add(tmp);
				System.out.println("** add: "+tmp);
			}
		}
		
//		System.out.println("** ending static method");
		
		return files;
	}

}//{c}Util