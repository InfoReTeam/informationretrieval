//package condorcetFuseIndexes;

import java.util.Vector;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Collection;
import java.util.Arrays;
import java.util.LinkedList;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList; 
import java.lang.System;
import java.io.Console;
import java.io.PrintWriter;

/*
 * Condorcet less efficient with some changes added with respect to the original code 
 * 
 * NEW numero query variabile
 *
 * @author T.Agnolazza
 * @version 0.3
*/

public class CondorcetFuseObjects
{
	private static int dimRes = 1000;
	private static int numDocs = 528155;
	private static int firstQuery = 351;
	private static int lastQuery = 400;
	private static int numQueries = lastQuery - firstQuery +1;
	private static boolean compareTimes = true;
	private static String absPath = "/Users/tommy/Projects/informationretrieval/INF_RET_Project_2017_2018/file.res/";
	
	public static void main(String[] args)
	{
		Vector<String> files = null;

		try{
			files = Util.fileInFolder(absPath);
		}
		catch(IOException ie){
			ie.printStackTrace();
			System.exit(1);
		}
		int numRuns = files.size();

		ArrayList<HashMap<String,Document>> queryHeavy = new ArrayList<HashMap<String,Document>>();
		for(int i = 0; i < numQueries; i++) {
			HashMap<String, Document> e = new HashMap<String, Document>();
			queryHeavy.add(e);
		}	
		
		// Inserimento dei ranking dei documenti nell'array
		File[] runs = new File[files.size()];
		for(int i = 0; i < files.size(); i++)
			runs[i] = new File(absPath + files.get(i));
		Scanner sc = null;
		Scanner scLine = null;
		
		for(int run = 0; run < runs.length; run++) 
		{
			int docPos = 0;
			
			// sc legge una run alla volta
			try	
			{
				sc = new Scanner(runs[run]);
			}
			catch(FileNotFoundException ioe) 
			{
				ioe.printStackTrace();
				System.exit(1);
			}
			
			while(sc.hasNextLine()) 
			{
				String tmp = sc.nextLine();
				scLine = new Scanner(tmp);
				
				String tmpNum = scLine.next();
				
				int queryNumber = Integer.parseInt(tmpNum);
				
				scLine.next(); // Skip Q0

				String docID = scLine.next();
				
				int rank = Integer.parseInt(scLine.next());
				
				Document currentDoc = null;
				HashMap<String, Document> currentQuery = queryHeavy.get(queryNumber - firstQuery);
				if(!currentQuery.containsKey(docID)) {
					currentDoc = new Document (docID, queryNumber, runs.length);
					currentQuery.put(docID, currentDoc);
				}
				else {
					currentDoc = currentQuery.get(docID);
				}
				
				docPos++;
				currentDoc.rank[run] = rank;
			}
		}//end for
		System.out.println("** Data acquired");

		
		ArrayList<Vector<Document>> query = new ArrayList<Vector<Document>>();
		for(int i = 0; i < numQueries; i++) {
			Vector<Document> tmp = new Vector<Document>();
			for(Document e : queryHeavy.get(i).values()) {
				tmp.add(e);
			}
			query.add(tmp);
		}
		
		//***********************************************************************************
		// Stampa diagnostica per controllo struttura dati
//		for(int i = 0; i < numQueries; i++){
//			System.out.println("Query "+query.get(i).get(0).IDQuery);
//			for(int j = 0; j < query.get(i).size(); j++){
//				if(query.get(i).size() > j){
//					Document doc = query.get(i).get(j);
//					System.out.print("\tDoc "+doc.IDDoc+": ");
//					for(int k = 0; k < numRuns; k++){
//						System.out.print(" "+doc.rank[k]+" ");
//					}
//					System.out.print("\n");
//				}
//			}
//		}
	    //***********************************************************************************
		
		
		// Riordino le query
		for(int i = 0; i < numQueries; i++)
		{
			Vector<Document> ord = sort(query.get(i));
			query.remove(i);
			query.add(i, ord);
		}
		System.out.println("** RankFusion conclusa");
		
		//Calcolo le componenti fortemente connesse
		for(int i = 0; i < numQueries; i++) {
			
			//Acquisizione degli elementi ordinati
			Vector<Document> currentQuery = query.get(i);
			LinkedList<Node> graph = new LinkedList<Node>();
			for(int j = 0; j< currentQuery.size(); j++) {
				graph.add(new Node(currentQuery.get(j),j));
			}
			
			compareTimes = true;
			//Prima chiamata alla DFS
			graph = DFS(graph, false);
			graph.sort(null);
			for(int j = 0; j< graph.size(); j++) {
				graph.get(j).colour = Node.Status.white;
			}

			//Chiamata alla DFS su grafo trasposto
			compareTimes = false;
			graph = DFS(graph, true);
			graph.sort(null);
			
			System.out.println("** Componenti connesse calcolate");
			System.out.println("** Query :"+(i+firstQuery));
			int component = graph.get(0).component;
			int dimComponent = 1;
			for(int j = 1; j< graph.size(); j++) {
				Node current = graph.get(j);
				if(current.component != component) {
					System.out.println("** Componente: "+component+", dimensione :"+dimComponent);
					component = current.component;
					dimComponent = 1;
				}
				else {
					dimComponent++;
				}
			}
			System.out.println("** Componente: "+component+", dimensione :"+dimComponent);
		}
		
		ArrayList<Tuple> arrayList = fromQueryArrayToArrayList(query);  
        Util.fromTupleArrayListToTRECFormat(arrayList, "CondorcetFuseV2" , "OutputCondorcetFuseV2.res");

		//***********************************************************************************
		// System.out.println("\n\n***** SORTED *****");
		// Stampa diagnostica per controllo struttura dati
//        for(int i = 0; i == numQueries; i++){
//			System.out.println("Query "+query.get(i).get(0).IDQuery);
//			for(int j = 0; j < query.get(i).size(); j++){
//				if(query.get(i).size() > j){
//					Document doc = query.get(i).get(j);
//					System.out.print("\tDoc "+doc.IDDoc+": ");
//					for(int k = 0; k < numRuns; k++){
//						System.out.print(" "+doc.rank[k]+" ");
//					}
//					System.out.print("\n");
//				}
//			}
//		}
		//***********************************************************************************
		
	}//[m]main 
	
	
	/*
	 * Metodo per fare il sorting che rileva le componenti connesse (complessità O(n^2))
	 * 
	 * @version 0.1
	 */
	private static <T extends Comparable<T>> Vector<T> sort(Vector<T> vec){	
		//lista ordinata
		LinkedList<T> orderedVec = new LinkedList<T>();
		
		//Insertion sort
		for(T current : vec) {
			int i = 0;
			while(i < orderedVec.size()) {
				T tmp = orderedVec.get(i); 
				if(current.compareTo(tmp) < 0) {
					orderedVec.add(i, current);
					break;
				}
				i++;
			}
			if(i >= orderedVec.size()) {
				orderedVec.addLast(current);
			}	
		}
		
//		for (int i = 0; i < orderedVec.size() -1; i++) {
//			T first = orderedVec.get(i);
//			T last = orderedVec.get(i+1);
//			if(first.compareTo(last) > 0) {
//				System.out.println("++ error: i"+i);				
//			}
//		}
		Vector<T> ret = new Vector<T>();
		while(!orderedVec.isEmpty())
			ret.add(orderedVec.removeFirst());
		
		return ret;
	}
	
	private static int time = 0;
	private static int component = 0;
	private static LinkedList<Node> DFS (LinkedList<Node> ll, boolean transpose){
		component = 0;
		time = -1;
		for(int i = 0; i < ll.size(); i++) {
			Node node = ll.get(i);
			if(node.colour.equals(Node.Status.white)) {
				if(transpose) {
					DFSVisitTranspose(ll, node);
					component++;
				}
				else {
					DFSVisit(ll, node);
				}
			}
		}
		return ll;
	}
	
	private static void DFSVisit (LinkedList<Node> ll, Node current) {
		time++;
		current.start = time;
		current.colour = Node.Status.grey;
		
		for(int i = 0; i < ll.size(); i++) {
			Node node = ll.get(i);
			int comp = (current.doc).compareTo(node.doc);
			if ((comp <= 0) && ((node.colour).equals(Node.Status.white))) {
				DFSVisit(ll, node);
			}
		}
		
		current.colour = Node.Status.black;
		time++;
		current.end = time;
	}
	
	private static void DFSVisitTranspose (LinkedList<Node> ll, Node current) {
		time++;
		current.start = time;
		current.colour = Node.Status.grey;
		current.component = component;
		
		for(int i = 0; i < ll.size(); i++) {
			Node node = ll.get(i);
			int comp = -((current.doc).compareTo(node.doc));

			if ((comp <= 0) && ((node.colour).equals(Node.Status.white))) {
				DFSVisitTranspose(ll, node);
			}
		}
		
		current.colour = Node.Status.black;
		time++;
		current.end = time;
	}
	
	private static ArrayList<Tuple> fromQueryArrayToArrayList(ArrayList<Vector<Document>> queryArr)
	{
		ArrayList<Tuple> res = new ArrayList<Tuple>(); 

		for(int k = 0; k < queryArr.size(); k++)
		{
			Vector<Document> tmpQuery = queryArr.get(k); 
			Vector<Document> resVec = new Vector<Document>();
			for(int i=0; i < tmpQuery.size(); i++) {
				resVec.add(i, tmpQuery.get(i));
			}

			for(int j = 0; j < resVec.size(); j++)
			{
				res.add(new Tuple(resVec.get(j).IDDoc, tmpQuery.get(0).IDQuery));   
			}
		}

	    return res; 

	}//[m]fromQueryArrayToArrayList

	
	/*
	 * Classe di supporto al calcolo delle componenti connesse
	 */
	static class Node implements Comparable<Node>{
		private enum Status {white, grey, black};
		private Status colour = null;
		private int start = -1;
		private int end = -1;
		private Document doc;
		private int rank;
		private int component = -1;
		
		public Node (Document doc, int rank) {
			this.doc = doc;
			this.rank = rank;
			colour = Status.white;
		}
		
		public int compareTo(Node t) {
			if(compareTimes) {
				return end < t.end ? 1 : -1;
			}
			else {
				int ret = 0;
				if (component < t.component)
					ret = -1;
				else 
					ret = 1;
				return ret;
			}
		}
	}
	
	/*
	 * classe che contiene l'ID del documento e il rank dato dai vari sistemi
	 */
	static class Document implements Comparable<Document>
	{
		private int IDQuery;
		private String IDDoc;
		private int[] rank;
		
		public Document(String actIDDoc, int actIDQuery, int numRuns)
		{
			IDDoc = actIDDoc;
			IDQuery = actIDQuery;
			rank = new int[numRuns];
			for(int i = 0; i < rank.length; i++) {
				rank[i] = dimRes+1;
			}
		}

		
		public int compareTo(Document o)
		{
			int thisDoc = 0;
			int otherDoc = 0;
			
			for(int i = 0; i < rank.length; i++)
			{
				if (this.rank[i] == o.rank[i]) {
					
				}
				else if(this.rank[i] < o.rank[i]) 
				{
					otherDoc++;
				}
				else 
				{
					thisDoc++;
				}
			}
			
			int result = 0;
			if(thisDoc < otherDoc)
				result = -1;
			else if (thisDoc > otherDoc)
				result = 1;
			return result;
		}
	}//{c}Document
}//{c}CondorcetFuseV2
