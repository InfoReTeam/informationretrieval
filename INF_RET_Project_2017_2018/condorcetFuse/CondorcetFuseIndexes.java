package condorcetFuseIndexes; 
import java.io.*; 
import java.util.*; 

/**
*@Version 4.0 algoritmo CondorcetFuse tramite array bidimensionale + calcolo dei cicli 
*@Author Ciresola Alessandro 
*@Date 25/11/2017
*@Update 22/12/2017 
*@Update 03/01/2018   
**/

public class CondorcetFuseIndexes
{
  //Struttura dati per l'inserimento dei rank
  private static int[][] array = null; // lo rendo visibile a tutti i metodi della classe, lo inizializzo nel main 
  private static final int numberOfRuns = 10; 
  private static final int numberOfQueries = 50; 
  private static final int numberOfDocuments = 528155; 
  private static final int numInizialeQuery = 351; 
  private static final int numFinaleQuery = 400; 
  //Tabella di mapping, serve per la mappatura (Tuple, Row), eseguita per motivi di efficienza in fase di lettura e scrittura 
  private static HashMap<Tuple, Integer> mappingTab = new HashMap<Tuple, Integer>(); 
  
  public static void main(String[] args) 
  {
    //Contatore di riga per array[][]
    int counter = 0; 
    
    //Inizializzazione strutture dati 
    int numberOfRows = (numberOfDocuments * numberOfQueries); 
    array = new int[numberOfRows][numberOfRuns]; 
     
    // Inizializzo tutti i campi dell'array bidimensionale a -1 
    for(int i = 0; i < numberOfRows; i++)
    {
    	for(int j=0; j < numberOfRuns; j++)
    	{
    		array[i][j] = -1 ; 
    	}
    }
    
    // Vettore di liste di documenti, una lista per query, ciascuna lista verrà poi riordinata tramite quickSort 
    // lista ordinata di tuple per la query numInizialeQuery --> vector.get(0)
    // lista ordinata di tuple per la query numFinaleQuery --> vector.get(vector.size()-1) etc... 
    
    Vector<ArrayList<Tuple>> vector = new Vector<ArrayList<Tuple>>(); 
    
    //Inizializzazione del vettore 
    for(int j = 0; j < numberOfQueries; j++)
    {
      vector.add(new ArrayList<Tuple>()); 
    }

    //Importazione delle RUN ottenute tramite Terrier 
    File[] runs = new File[numberOfRuns]; 
    String absPath = "/home/alessandro/Scrivania/file.res/";
    //String absPath = "/home/alessandro/Documenti/condorcet2002/runs/TREC5/automatic/"; 
    /*
    String path0 = abspath + "1_BM25/BM25b0.75_0.res";
    runs[0] = new File(path0); 
    String path1 = abspath + "2_TF_IDF/TF_IDF_0.res";
    runs[1] = new File(path1); 
    String path2= abspath + "11_DFR_BM25/DFR_BM25c1.0_1.res";
    runs[2] = new File(path2); 
    String path3 = abspath + "4)Hiemstra_LM/Hiemstra_LM0.15_0.res";
    runs[3] = new File(path3); 
    String path4 = abspath + "5)DirichletLM/DirichletLM_0.res";
    runs[4]= new File(path4); 
    String path5 = abspath + "6)BM25/BM25b0.75_01.res";
    runs[5] = new File(path5); 
    String path6 = abspath + "7_TF_IDF/TF_IDF_01.res";
    runs[6] = new File(path6); 
    String path7 = abspath + "12_Tf/Tf_0.res";
    runs[7] = new File(path7); 
    String path8 = abspath + "9_Hiemstra_LM/Hiemstra_LM0.15_0.res"; 
    runs[8] = new File(path8); 
    String path9 = abspath + "10_DirichletLM/DirichletLM_0.res"; 
    runs[9] = new File(path9);      
    */
    
    Vector<String> files = null;
    // Inserimento dei ranking dei documenti nell'array
    try
    {
      files = Util.fileInFolder(absPath);
    }
    catch(IOException ie)
    {
      ie.printStackTrace();
      System.exit(1);
    }
    
    //numRuns = files.size();
    //File[] runs = new File[files.size()];
    for(int i = 0; i < files.size(); i++)
      runs[i] = new File(absPath + files.get(i));
    

    Scanner sc = null; 
    Scanner scLine = null; 

    //Inizializzazione scanner, un oggetto della classe Scanner per RUN 
    for(int j = 0; j < numberOfRuns; j++)
    {
      try
      {
        sc = new Scanner(runs[j]); 
      }
      catch(FileNotFoundException fnf)
      {
        fnf.printStackTrace(); 
      }
      
      while(sc.hasNextLine())
      {
        String nextLine = sc.nextLine();
        scLine = new Scanner(nextLine);  
        //Number of topic/query
        int queryNum = Integer.parseInt(scLine.next()); //DA MODIFICARE QUANDO VIENE FATTO GIRARE SULLA COLLEZIONE TREC!!!!!! 
        //skip Q0
        scLine.next(); 
        //DocumentID
        String docID = scLine.next(); 
        //Rank del documento 
        int rank = Integer.parseInt(scLine.next()); 

        Tuple tuple = new Tuple(docID, queryNum); 
        if(mappingTab.containsKey(tuple)) // devo solo aggiornare l'array
        {
          int row = mappingTab.get(tuple).intValue(); 
          array[row][j] = rank; 
        }
        else //devo eseguire un mapping inserendo la nuova tupla 
        {
          array[counter][j] = rank; 
          //mapping 
          mappingTab.put(tuple, Integer.valueOf(counter)); 
          //Puntatore alla prossima riga vuota dell'array
          counter++; 
          //Essendo una tupla nuova che non ho mai letto, essa rappresenta un possibile candidato per il rank fusion 
          // di conseguenza la aggiungo alla lista appropriata di documenti candidati per la query i-esima
          vector.get(queryNum-numInizialeQuery).add(tuple);
        }//endElse
      }
    }
     
    //Arrivati a questo punto vector contiene una sequenza di liste, una per ciascuna query, con le tuple candidate al 
    //rank fusion, ovvero con tutte e sole le tuple che sono comparse in almeno una RUN! 
    //Trasformo gli ArrayList in array ed applico su ciascuno di essi il quicksort ottenendo così per ciascuna 
    // query la lista di tuple ordinate dalla più rilevante(a sinistra) alla meno rilevante(a destra) 
    
    Vector<Tuple[]> resVec = new Vector<Tuple[]>(); 
    
    for(int t = 0; t < vector.size(); t++)
    {
      Object[] tpObj = vector.get(t).toArray();
      Tuple[] tp = new Tuple[tpObj.length]; 

      for(int k = 0; k < tpObj.length; k++)
      { 
        tp[k] = (Tuple)(tpObj[k]);    
      }
      quickSort(tp, 0, tp.length-1); 
      resVec.add(t, tp);  
    }
    
    
    /* 
    //********************************************************
    //**************STAMPA DIAGNOSTICA************************
    //********************************************************
    Tuple[] tp = resVec.get(9); 
    System.out.println("Numero Query: " + tp[0].getQueryId()); 
     
    for(int t = 0; t < 10; t++)
    {
      int row = mappingTab.get(tp[t]).intValue(); 

      for(int r = 0; r < 9; r++)
      { 
        System.out.print("  " + array[row][r] + "  ");  
      }
      System.out.println("  " + array[row][9] + "  "); 
    }
    //********************************************************
    //********************************************************
    */ 
     
    //Output to TREC Format 
    Util.fromVecToTRECFormat(resVec, "CondorcetFuseIndexes" , "condorcetFuseIndexesOutput.res");
    
    //***************************************************************************************
    //Debugging
    //***************************************************************************************
    /*
    for(int j = 0; j < resVec.size(); j++)
    {
      Tuple[] tpl = resVec.get(j); 
      boolean repeat = true; 

      int count = 0; 
      for(int i = 1; i < tpl.length; i++)
      {
        if(compare(tpl[i-1], tpl[i]) > 0) //ERRORE!!!!!
        {
          System.out.println("WHY??????"); 
        }
      }
    }
    */
    //**************************************************************************************
    
    //Tutti gli elementi sono ordinati quindi k1 <= k2 <= k3 <= ... <= kn secondo la funzione di compare 
    //definita in precedenza 

    //Calcolo delle componenti connesse + stampa  
    ArrayList<LinkedList<Tuple>> cicles = null; 
    
    for(int j = 0; j < resVec.size(); j++)
    {
      cicles = new ArrayList<LinkedList<Tuple>>(); 
      Tuple[] tupleArr = resVec.get(j); 
      //ArrayList<Integer> notStamp = new ArrayList<Integer>(); 
      int init = 0; 
      
      for(int current = 0; current < tupleArr.length; current++)
      {
        if(tupleArr.length <= 2) //Da mettere giu' meglio!!!!
        {
          break; 
        }
        else
        {
          Tuple tp = tupleArr[current];

          if(cicles.size() == 0)
          {
            LinkedList<Tuple> linked = new LinkedList<Tuple>(); 
            linked.addFirst(tp);
            cicles.add(current, linked);  
          }
          else 
          {
            int index = -1; 

            for(int i = 0; i < current; i++) //for(int i = init; ...) 
            {
              if(compare(tp, tupleArr[i]) <= 0)
              {
                index = i; 
                break;
              } 
            }

            if(index == -1)
            {
              LinkedList<Tuple> linked = new LinkedList<Tuple>(); 
              linked.addFirst(tp);
              cicles.add(current, linked);

              if((current+1) == tupleArr.length) //Sono a fine lista... 
              { 
                int actQuery = tp.getQueryId();
                int count = 0; 
                System.out.println("Cicli per la Query : " + actQuery); 
                int cicleCounter = 1; 
                int numCycles = 0; 
                int numGreaterThan20 = 0; 

                for(int q = 0; q < cicles.size(); q++) 
                {
                  LinkedList<Tuple> linke = cicles.get(q); 

                  //if(linked.size() != 0 && cicleCounter <= 10) 
                  //if(linke.contains(tupleArr[q]))  
                  //if(!notStamp.contains(Integer.valueOf(q)))   
                  
                  if(!linke.getFirst().getDocumentId().equals("NotDocument")) 
                  {
                    //System.out.println(linked.getFirst().getDocumentId() + " , " + linked.getFirst().getQueryId()); 
                    System.out.println("Ciclo " + (cicleCounter++) + " di dimensione: " + linke.size());  
                  }

                  if(linke.size() > 1) 
                  {
                    count = count + linke.size(); 
                    numCycles++; 
                    if(linke.size() > 20)
                    {
                      numGreaterThan20++; 
                    }
                  }
                }

                System.out.println("Numero di cicli per la query " + actQuery + " : " + numCycles); 
                System.out.println("Numero di cicli/numero totale documenti : " + (((double)numCycles)/tupleArr.length)*100 + " %");
                System.out.println("Numero di cicli di taglia > 20 per la query " + actQuery + " : " + numGreaterThan20);
                System.out.println("Percentuale di cicli significativi per la query " + actQuery + " : " + (((double)numGreaterThan20)/numCycles)*100 + " %");
                System.out.println("Numero totale di documenti nei cicli: " + count + " su " + tupleArr.length); 
                System.out.println("Percentuale di documenti all'interno di cicli : " + (((double)count)/tupleArr.length)*100 + " %");
              }
            }
            else //index == i 
            {
              if((current+1) < tupleArr.length) //Non sono a fine lista...
              {
              
                //**********************************************************************************
                //****************************NEW***************************************************
                //**********************************************************************************
                //Creo una tupla fittizia formata da DocumentId e QueryID dove 
                // in QueryID salvo index, puntatore alla posizione in cui vi e' salvato il ciclo 
                // in docID metto il docID della tupla iniziale che ha dato origine al ciclo 
                //**********************************************************************************
                
                //Se tupleArr[index] e' gia' contenuto all'interno di un ciclo 
                if((cicles.get(index).size()==1) && (cicles.get(index).getFirst().getDocumentId().equals("NotDocument")))    
                {
                  int pointerToCycle = cicles.get(index).getFirst().getQueryId(); 

                  for(int k = pointerToCycle+1; k < current; k++)
                  {
                    if(cicles.get(k).contains(tupleArr[k]))
                    {
                      while(cicles.get(k).size() != 0)  
                      {
                        cicles.get(pointerToCycle).addLast(cicles.get(k).removeFirst());  
                      }
                      cicles.get(k).addFirst(new Tuple("NotDocument", pointerToCycle)); 
                      //notStamp.add(Integer.valueOf(k)); 
                    }
                    else
                    {
                      cicles.get(k).removeFirst(); 
                      //Reindirizzo su quell'elemento che va a costituire un ciclo piu' grande 
                      cicles.get(k).addFirst(new Tuple("NotDocument" , pointerToCycle)); 
                    }
                  }

                  cicles.get(pointerToCycle).addLast(tupleArr[current]); 
                  //notStamp.add(Integer.valueOf(current)); 
                  LinkedList<Tuple> link = new LinkedList<Tuple>(); 
                  link.addFirst(new Tuple("NotDocument", pointerToCycle));  
                  cicles.add(current, link); 
                }
                else
                {
                  for(int k = index+1; k < current; k++)
                  {
                    //Per tutti gli elementi che non sono stati reindirizzati... 
                    if(cicles.get(k).contains(tupleArr[k]))   
                    {
                      while(cicles.get(k).size() != 0)   
                      {
                        cicles.get(index).addLast(cicles.get(k).removeFirst()); 
                      }
                      cicles.get(k).addFirst(new Tuple("NotDocument", index)); 
                      //notStamp.add(Integer.valueOf(k)); 
                    }
                    else
                    {
                      cicles.get(k).removeFirst(); 
                      //Reindirizzo su quell'elemento che va a costituire un ciclo piu' grande 
                      cicles.get(k).addFirst(new Tuple("NotDocument" , index)); 
                    }
                  }

                  cicles.get(index).addLast(tupleArr[current]); 
                  //notStamp.add(Integer.valueOf(current)); 
                  LinkedList<Tuple> link = new LinkedList<Tuple>(); 
                  link.addFirst(new Tuple("NotDocument", index)); 
                  cicles.add(current, link);  
                } 
              }
              else //lista terminata! 
              {
                //Se tupleArr[index] e' gia' contenuto all'interno di un ciclo 
                if((cicles.get(index).size()==1) && (cicles.get(index).getFirst().getDocumentId().equals("NotDocument")))     
                {
                  int pointerToCycle = cicles.get(index).getFirst().getQueryId(); 

                  for(int k = pointerToCycle+1; k < current; k++)
                  {
                    if(cicles.get(k).contains(tupleArr[k]))
                    {
                      while(cicles.get(k).size() != 0)    
                      {
                        cicles.get(pointerToCycle).addLast(cicles.get(k).removeFirst()); 
                      }
                      cicles.get(k).addFirst(new Tuple("NotDocument" , pointerToCycle)); 
                      //notStamp.add(Integer.valueOf(k)); 
                    }
                    else
                    {
                      cicles.get(k).removeFirst(); 
                      //Reindirizzo su quell'elemento che va a costituire un ciclo piu' grande 
                      cicles.get(k).addFirst(new Tuple("NotDocument" , pointerToCycle)); 
                    }
                  }

                  cicles.get(pointerToCycle).addLast(tupleArr[current]); 
                  //notStamp.add(Integer.valueOf(current)); 
                  LinkedList<Tuple> link = new LinkedList<Tuple>();                        
                  link.addFirst(new Tuple("NotDocument" , pointerToCycle)); 
                  cicles.add(current, link); 
                }
                else
                {
                  for(int k = index+1; k < current; k++)
                  {
                    //Per tutti gli elementi che non sono stati reindirizzati... 
                    if(cicles.get(k).contains(tupleArr[k]))    
                    {
                      while(cicles.get(k).size() != 0)     
                      {
                        cicles.get(index).addLast(cicles.get(k).removeFirst()); 
                      }
                      cicles.get(k).addFirst(new Tuple("NotDocument", index));   
                      //notStamp.add(Integer.valueOf(k)); 
                    }
                    else
                    {
                      cicles.get(k).removeFirst(); 
                      //Reindirizzo su quell'elemento che va a costituire un ciclo piu' grande 
                      cicles.get(k).addFirst(new Tuple("NotDocument" , index)); 
                    }
                  }

                  cicles.get(index).addLast(tupleArr[current]); 
                  LinkedList<Tuple> link = new LinkedList<Tuple>(); 
                  link.addFirst(new Tuple("NotDocument", index));  
                  cicles.add(current, link); 
                }


                //Stampo i cicli dal momento che la lista corrente è terminata 
                int actQuery = tp.getQueryId();
                int count = 0; 
                System.out.println("Cicli per la Query : " + actQuery); 
                int cicleCounter = 1; 
                int numCycles = 0; 
                int numGreaterThan20 = 0; 
                
                //for(LinkedList<Tuple> linked : cicles) 
                for(int x = 0; x < cicles.size(); x++) 
                {
                  LinkedList<Tuple> linked = cicles.get(x); 
                  
                  if(!linked.getFirst().getDocumentId().equals("NotDocument"))  
                  {
                    System.out.println("Ciclo " + (cicleCounter++) + " di dimensione: " + linked.size());  
                  }

                  if(linked.size() > 1) 
                  {
                    count = count + linked.size(); 
                    numCycles++; 
                    if(linked.size() > 20)
                    {
                      numGreaterThan20++; 
                    }
                  }
                }
                System.out.println("Numero di cicli per la query " + actQuery + " : " + numCycles); 
                System.out.println("Numero di cicli/numero totale documenti : " + (((double)numCycles)/tupleArr.length)*100 + " %");
                System.out.println("Numero di cicli di taglia > 20 per la query " + actQuery + " : " + numGreaterThan20);
                System.out.println("Percentuale di cicli significativi per la query " + actQuery + " : " + (((double)numGreaterThan20)/numCycles)*100 + " %");
                System.out.println("Numero totale di documenti nei cicli: " + count + " su " + tupleArr.length); 
                System.out.println("Percentuale di documenti all'interno di cicli : " + (((double)count)/tupleArr.length)*100 + " %");
              }
            }
          }
        }
      }
    }

  }//[m]main



//Implementazione del QuickSort in loco 
public static void quickSort(Tuple[] arr, int left, int right) 
{
  if(left >= right) 
    return; 

  Tuple temp = null; //elemento usato per lo scambio di posto 
  Tuple pivot = arr[right]; 
  int i = left; 
  int j = right-1; 

  while(i <= j)
  {
    while((i <= j) && (compare(arr[i] , pivot) <= 0))
      i++; 

    while((j >= i) && (compare(arr[j],pivot) >= 0))
      j--; 

    if(i < j)
    {
      temp = arr[j]; 
      arr[j] = arr[i]; 
      arr[i] = temp; 
    }
  } 

  //Il loop e' terminato in quanto gli indici si sono incrociati 
  //Scambio il pivot con quanto contenuto in i 
  temp = arr[right]; 
  arr[right] = arr[i]; 
  arr[i] = temp; 
  
  quickSort(arr, left, i-1); 
  quickSort(arr, i+1 , right); 

}//[m]quickSort 


public static int compare(Tuple tp1, Tuple tp2)
{
  int count = 0; 
  int rowOftp1 = mappingTab.get(tp1).intValue();
  int rowOftp2 = mappingTab.get(tp2).intValue(); 

  for(int t=0; t < numberOfRuns; t++)
  {    
    if((array[rowOftp1][t] == -1) || (array[rowOftp2][t] == -1))
    {
      if(array[rowOftp1][t] == array[rowOftp2][t]) 
      {}
      else if(array[rowOftp1][t] == -1) 
      {
        count--; //vince tp2 
      }
      else if(array[rowOftp2][t] == -1)
      {
        count++; //vince tp1 
      }
    }
    else 
    {
      if(array[rowOftp1][t] < array[rowOftp2][t]) //tp1 vince rispetto a tp2 
      {
        count++; 
      }
      else if(array[rowOftp1][t] > array[rowOftp2][t]) //tp2 vince rispetto a tp1 
      {
        count--; 
      }
    }
  }//end for

  if(count > 0) //tp1 < tp2  
  {
    return -1; 
  }
  else if(count == 0) //tp1 = tp2 
  {
    return 0; 
  }
  else //tp1 > tp2 
  {
    return 1; 
  }
}//[m]compare 

}//{c} CondorcetFuse 
