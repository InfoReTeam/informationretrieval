import java.io.*;
import java.util.Scanner;

public class ToolsAnalysis {
	
	public static void main(String[] args) {
		String path = "/Users/tommy/Projects/informationretrieval/qrels.trec7.txt";
		
		int[] res = maxRelevantDocs(path);
		System.out.println("La query con il numero massimo di documenti rilevanti è: "+ res[0]+ " con "+res[1]+" documenti.");
	}
	
	
	/*
	 * Find out the query with the maximum number of relevant docs
	 * 
	 * @author T.Agnolazza
	 * @version 1.0
	 * @return the number of the query
	 */
	public static int[] maxRelevantDocs(String path) {
		Scanner file = null;
		int[] res = new int[2];
		
		try{
			file = new Scanner(new FileInputStream(path));
		}
		catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			System.exit(1);
		}
		
		int query = -1;
		int relevant = 0;
		while(file.hasNextLine()) {
			Scanner line = new Scanner(file.nextLine());
			
			int tmp = line.nextInt();
			if(tmp != query) {
				if(relevant > res[1]) {
					res[1] = relevant;
					res[0] = query;
				}
				query = tmp;
				relevant = 0;
			}
			
			//Ignoring the 0
			line.next();
			//Ignoring the doc
			line.next();
			
			tmp = line.nextInt();
			if(tmp > 0) {
				relevant++;
			}
		}
		
		return res;
	}
}
