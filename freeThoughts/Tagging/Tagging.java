import edu.stanford.nlp.util.logging.Redwood;
import java.util.Scanner; 
import java.io.*; 
import java.util.Hashtable; 
import java.util.List; 
import java.io.FileNotFoundException; 
import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import java.util.regex.*; 
import java.util.ArrayList; 


/*
 * Trial class for tagging
 * 
 * @author A.Ciresola
 * @version 0.1
 */
public class Tagging
{   
	// private static Redwood.RedwoodChannels log = Redwood.channels(Tagging.class);
	public static Pattern pattern = Pattern.compile("^[',\\.`/-_]+$"); 

	public static void main(String[] args)
	{
        MaxentTagger tagger = new MaxentTagger("models/english-left3words-distsim.tagger");

        TokenizerFactory<CoreLabel> ptbTokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(),"untokenizable=noneKeep");
        BufferedReader fileRead = null; 
		
		try
		{
            fileRead = new BufferedReader(new InputStreamReader(new FileInputStream("example_inputText.text"), "utf-8")); 
    	}catch(FileNotFoundException ie)
    	{
    		ie.printStackTrace(); 
    	}catch(UnsupportedEncodingException ex) 
    	{
    		ex.printStackTrace(); 
    	}

        DocumentPreprocessor documentTokenized = new DocumentPreprocessor(fileRead);
        documentTokenized.setTokenizerFactory(ptbTokenizerFactory); 

        ArrayList<List<TaggedWord>> tokens = new ArrayList<List<TaggedWord>>(); 

        for(List<HasWord> sentence : documentTokenized)
        {    
            ArrayList<HasWord> tmp = new ArrayList<HasWord>(); 

        	    sentence.forEach( (item) -> 
        		{
        			// System.out.print(item.word() + " "); 
        			if(!pattern.matcher(item.word()).matches()) 
        			{
        			   tmp.add(item); 
        			}
        		}); 

        	List<TaggedWord> tag = tagger.tagSentence(tmp); 
            tokens.add(tag); 
        }

        
        tokens.forEach((list)->
        	{   
        		list.forEach((tag)->
        			{
        				System.out.println(tag.toString());
        			}); 
        	});
        
        
        try
        {
          fileRead.close(); 
    	}catch(IOException ee)
    	{
    		ee.printStackTrace(); 
    	}
     
    	/*
    	ArrayList<TaggedWord> taggedWords = new ArrayList<TaggedWord>(); 
        tokens.forEach((arg)->
        	{

        	});
        */



	}
}//{c}Testing
