import edu.stanford.nlp.util.logging.Redwood;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class TaggerDemo2Modified {

	/** A logger for this class */
  private static Redwood.RedwoodChannels log = Redwood.channels(TaggerDemo2.class);

  // costruttore della classe 
  private TaggerDemo2Modified() 
  {}


  public static void main(String[] args) throws Exception 
  {
  	
  	MaxentTagger tagger = new MaxentTagger("models/wsj-0-18-left3words-distsim.tagger"); 
  	
    TokenizerFactory<CoreLabel> ptbTokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(),"untokenizable=noneKeep"); 

    





  }//[m]main

}//{c}TaggerDemo2Modified