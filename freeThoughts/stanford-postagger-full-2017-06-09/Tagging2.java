import edu.stanford.nlp.util.logging.Redwood;
import java.util.Scanner; 
import java.io.*; 
import java.util.Hashtable; 
import java.util.List; 
import java.io.FileNotFoundException; 
import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import java.util.regex.*; 
import java.util.ArrayList; 
import java.util.HashSet; 
import java.util.Arrays; 
import java.util.Enumeration; 



/*
 * Trial class for tagging
 * 
 * @author A.Ciresola
 * @version 0.1
 */


public class Tagging2 
{   
    // channel for the log of the file 
    private static Redwood.RedwoodChannels log = Redwood.channels(Tagging2.class);
	public static Pattern pattern = Pattern.compile("^([{',\\.`/-_}])+$"); 
    public static HashSet<String> tokens = new HashSet<>(Arrays.asList("-lsb-", "-rsb-", "-lrb-", "-rrb-", "'s", "--"));


	public static void main(String[] args)
	{ 
        Scanner sc = null; 
        MaxentTagger tagger = new MaxentTagger("models/wsj-0-18-left3words-distsim.tagger"); 

        TokenizerFactory<CoreLabel> ptbTokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(),"untokenizable=noneKeep");
        BufferedReader fileRead = null; 

        Hashtable<String, Integer> hashtable = new Hashtable<String, Integer>();  
		
		try
		{
            fileRead = new BufferedReader(new InputStreamReader(new FileInputStream("example_inputText.text"), "utf-8")); 
    	}catch(FileNotFoundException ie)
    	{
    		ie.printStackTrace(); 
    	}catch(UnsupportedEncodingException ex) 
    	{
    		ex.printStackTrace(); 
    	}

        DocumentPreprocessor documentTokenized = new DocumentPreprocessor(fileRead);
        documentTokenized.setTokenizerFactory(ptbTokenizerFactory); 

        ArrayList<List<TaggedWord>> tokens = new ArrayList<List<TaggedWord>>(); 

        for(List<HasWord> sentence : documentTokenized)
        {    
            ArrayList<HasWord> tmp = new ArrayList<HasWord>(); 

        	    sentence.forEach( (item) -> 
        		{
        			// System.out.print(item.word() + " "); 
        			if(!pattern.matcher(item.word()).matches() && !tokens.contains(item) && !item.toString().equals("-LRB-") && !item.toString().equals("-RRB-")) 
        			{   
                       String w = item.word(); 
                       item.setWord(w.toLowerCase()); 
        			   tmp.add(item); 
        			}
        		}); 

        	List<TaggedWord> tag = tagger.tagSentence(tmp); 
            tokens.add(tag); 
        }

        
        // Calcolo delle frequenze relative 
        tokens.forEach((list)->
        	{   
        		    list.forEach((tag)->
        			{
                        // Se la parola non è presente nella tabella hash allora la inserisco mappandola in 1 = numero di occorrenze 
                        // in cui la parola compare nel documento 
                        if(!hashtable.containsKey(tag.toString())) 
                        {
                            hashtable.put(tag.toString(), 1 ); 
                        }
                        else 
                        {
                            // ottengo il numero attuale di occorrenze di quella parola 
                            Integer num = hashtable.get(tag.toString());  
                            //incremento tale numero di 1 
                            hashtable.replace(tag.toString(), num, (num+1));  
                        }

        			}); 
        	});
        

        // Debugging 
        Enumeration<String> enumeration = hashtable.keys(); 
        while(enumeration.hasMoreElements())
        {
            String key = enumeration.nextElement(); 
            System.out.println("Key: " + key + ", Frequency: " + hashtable.get(key));
        }
       



        try
        {
          fileRead.close(); 
    	}catch(IOException ee)
    	{
    		ee.printStackTrace(); 
    	}

             



	}//[m]main

}//{c}Tagging2